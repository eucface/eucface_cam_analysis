source("load.R")
source("R/functions.R")
source("R/functions-figures.R")

# load from cache


# 
# 
# part1 <- read.csv("output/FACE_RA_P0037_SECURPHOT-GCC_20141104-20150704.csv", stringsAsFactors = FALSE)
# part2 <- read.csv("output/FACE_RA_P0037_SECURPHOT-GCC_20150705-20150906.csv", stringsAsFactors = FALSE)
# 
# fgcc <- as.data.frame(bind_rows(part1, part2))
# fgcc$Date <- as.Date(fgcc$Date)


fgcc <- read.csv("output/FACE_RA_P0037_SECURPHOT-GCC_20141104-20160316.csv")
fgcc$Date <- as.Date(fgcc$Date)

# average four images per ring, per day
fgcca <- as.data.frame(summarize(group_by(fgcc, Ring, Date),
                   GCC=mean(GCC, na.rm=TRUE)))
fgcca$Ring <- as.factor(fgcca$Ring)


source("R/functions-figures.R")
palette(my_ringcols())
smoothplot(Date, GCC, Ring, data=fgcca, kgam=12)




# Zoomed
deleteBad <- function(df){
  
  df$GCC[df$Ring == 6 & df$Date > as.Date("2015-7-28") & df$Date < as.Date("2015-8-12")] <- NA
  
  return(df)
}
zoomed2 <- deleteBad(zoomed_res)


zoomed2$photo <- as.factor(paste(zoomed2$Ring, zoomed2$Label))

smoothplot(Date, GCC, photo, data=subset(zoomed2, Date > as.Date("2015-7-1")), 
           kgam=7, linecol=rep(my_ringcols(),each=2),
           pointcols=alpha(rep(my_ringcols(),each=2),0.2))




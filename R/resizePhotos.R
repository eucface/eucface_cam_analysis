#'@export
resizePhotos <- function(fn=NULL, path=NULL, width=1280, ask=TRUE){
  
  if(!is.null(path)){
    o <- getwd()
    on.exit(setwd(o))
    setwd(path)
  }
  
  r <- shell("convert",intern=TRUE)
#   if(!grepl("ImageMagick",r[1]))
#     stop("Install ImageMagick first! www.imagemagick.org")
#   
  if(is.null(fn)){
    warning("Resizing all JPG/JPEGs in the current path.")
    fn1 <- list.files(pattern="[.]jpg", ignore.case=TRUE)
    fn2 <- list.files(pattern="[.]jpeg", ignore.case=TRUE)
    fn <- c(fn1, fn2)
  }
    
  n <- length(fn)
  if(!all(file.exists(fn)))
      stop("One or more of provided file names do not exist.")
    
  if(ask){
    ans <- readline("This will overwrite your original files! Continue (y/n)?")
    if(!ans == "y")stop()
  }
  
  for(i in 1:n){
    cmd <- paste0("convert ", fn[i], " -resize ", width," ", fn[i])
    sh <- shell(cmd, intern=TRUE)
  }
}

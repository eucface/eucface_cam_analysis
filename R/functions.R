
my_ringcols <- function(){
  
  #   reds <- brewer.pal(5,"Reds")[3:5]
  #   blues <- brewer.pal(5,"Blues")[3:5]
  #   c(reds[1],blues[1:2],reds[2:3],blues[3])
  
  c("#FB6A4A","#6BAED6","#3182BD","#DE2D26","#A50F15","#08519C")
}


# Replacing function - CMN 2020-05-25
# NEW  get_eddy function

get_eddy <- function(){
  # Eddy
  #  eddy <- downloadTOA5("Eddyflux_slow_met", startDate="2014-12-1", 
  #                       maxnfiles=200, allowedExtensions=c(".dat"))
  
  # account for Smartflux implementation after 2018-09-5
  eddy_old = downloadTOA5("Eddyflux_slow_met", startDate = "2014-12-1", endDate = '2018-09-12 12:00:00',
                          maxnfiles = 200, allowedExtensions = c(".dat"), tryoffline = TRUE)
  eddy_old = eddy_old[, c('DateTime', 'PAR_Den', 'TotalSS', 'DiffuseSS', 'Source')]
  eddy_new = downloadTOA5("CUP_AUTO_climate", startDate = '2018-09-12 12:00:00', endDate = today(),
                          maxnfiles = 200, allowedExtensions = c(".dat"), tryoffline = TRUE)
  names(eddy_new)[names(eddy_new) == 'PAR_upwell_Avg'] = 'PAR_Den'
  names(eddy_new)[names(eddy_new) == 'TotalSS_Avg'] = 'TotalSS'
  names(eddy_new)[names(eddy_new) == 'DiffuseSS_Avg'] = 'DiffuseSS'
  eddy_new = eddy_new[, c('DateTime', 'PAR_Den', 'TotalSS', 'DiffuseSS', 'Source')]
  eddy = rbind(eddy_old, eddy_new)
  
  # don't actually have to aggregate; but this fixes duplicate entries quickly and safely
  eddyagg <- as.data.frame(dplyr::summarize(group_by(eddy,DateTime=nearestTimeStep(DateTime,30)),
                                            TotalSS=mean(TotalSS),
                                            DiffuseSS=mean(DiffuseSS),
                                            PAR_Den=mean(PAR_Den),
                                            Source_eddy=first(Source)))
  eddyagg$Fdiff <- with(eddyagg, DiffuseSS / TotalSS)
  
  return(eddyagg)
} 

# OLD FUNCTION
# get_eddy <- function(){
#   # Eddy
#   eddy <- downloadTOA5("Eddyflux_slow_met", startDate="2014-12-1", 
#                        maxnfiles=200, allowedExtensions=c(".dat"))
#   
#   # don't actually have to aggregate; but this fixes duplicate entries quickly and safely
#   eddyagg <- as.data.frame(dplyr::summarize(group_by(eddy,DateTime=nearestTimeStep(DateTime,30)),
#                                             TotalSS=mean(TotalSS),
#                                             DiffuseSS=mean(DiffuseSS),
#                                             PAR_Den=mean(PAR_Den),
#                                             Source_eddy=first(Source)))
#   eddyagg$Fdiff <- with(eddyagg, DiffuseSS / TotalSS)
#   
#   return(eddyagg)
# }

# add nearest reading to processed images
diffun <- function(d1,d2)abs(d1 - d2)

get_label_fn <- function(x){
  lab <- strsplit(basename(x),"_")[[1]][4]
  lab <- gsub("SECURPHOT-","",lab)
  as.character(gsub("-"," ",lab))
}

get_ring_fn <- function(x){
  rng <- strsplit(basename(x),"_")[[1]][2]
  as.numeric(substr(rng, 2,2))
}

# Get time fun.
tfn <- function(x){
  tm <- str_extract(x, "[0-9]{8}-[0-9]{2}-[0-9]{2}-[0-9]{2}")  
  as.POSIXct(tm, format="%Y%m%d-%H-%M-%S", tz="UTC")
}


analyze_gcc_day <- function(date, grep_str="Wide-view"){
  
  fns <- dir(.jpeg_path, pattern=format(date, "%Y%m%d"),
             recursive=TRUE, full.names=TRUE)
  fns <- fns[grep(grep_str, fns)]
  
  if(!length(fns))return(NULL)
  
  img <- setImages(fns=fns, getTimeFun=tfn)
  
  invisible(capture.output(res <- suppressMessages(processImages(img))))
  
  res$Camera <- unname(sapply(res$filename, get_label_fn))
  res$Ring <- unname(sapply(res$filename, get_ring_fn))
  
  DT <- data.table(res, key = "DateTime")
  tm <- data.table(eddy_diff, key = "DateTime")
  
  dfr <- as.data.frame(tm[DT, roll='nearest'], stringsAsFactors=FALSE)
  
  dfr
}



analyze_gcc_cache <- function(cachefile, first_date="2014-11-04", ...){
  
  # Read cache file unless it does not exist.
  if(!file.exists(cachefile)){
    gcc_old <- NULL
    dates <- seq(as.Date(first_date), today() - 1, by="1 day")
  } else {
    gcc_old <- readRDS(cachefile)
    old_dates <- sort(unique(as.Date(gcc_old$DateTime)))
    dates <- seq(max(old_dates), as.Date(today() - 1), by="1 day")
  }
  
  if(length(dates) > 1){
    
    # Get GCC for dates.
    gcc_new <- pblapply(dates, analyze_gcc_day, ...) %>%
      bind_rows
    
    if(!is.null(gcc_old)){
      out <- rbind(gcc_old, gcc_new)
    } else {
      out <- gcc_full_new
    }
    saveRDS(out, cachefile)
    
  } else {
    out <- gcc_old
  }
  
  return(out)
}


round_num <- function(x, dig=6){
  if(is.numeric(x))
    round(x, dig)
  else
    x
}


make_and_write_TOA5 <- function(data, fn, datevar="Date", datetimevar=NULL, logger_id){
  
  data$filename <- basename(as.character(data$filename))
  
  header <- paste(c("\"TOA5\"","\"FACE\"","\"none\"","\"none\"",
                    "\"none\"","\"none\"",
                    "\"none\"",paste0("\"", logger_id, "\"")), collapse=",")
  units <- paste(c("\"\"","\"YYYY-MM-DD\"","\"\"","\"\"","\"\"","\"mm\"",
                   "\"\"","\"\"","\"\"","\"\"","\"-\""),collapse=",")
  
  data$TIMESTAMP <- data[,datetimevar]
  data <- dplyr::select(data, TIMESTAMP, Date, Ring, Camera, GCC, RCC, BCC, RGBtot, TotalSS, Fdiff)
  
  nm <-  paste(paste("\"", names(data), "\"", sep=""), collapse=",")
  empty <- paste(paste("\"", rep("", ncol(data)), "\"", sep=""), collapse=",")
  writeLines(c(header, nm, units, empty ),   fn)
  
  # Round to 5 digits
  data <- as.data.frame(lapply(data, round_num, dig=5))
  
  write.table(data, fn, append=TRUE, col.names=FALSE, row.names=FALSE, sep=",", na="NAN")
}




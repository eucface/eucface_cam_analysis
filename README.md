# EucFACE code base : analysis of security camera photos for greenness

Greenness estimated from processing images collected by the security cameras at EucFACE. Images are collected daily (six photos per ring, three times around midday). All these images are processed to calculate the Green Chromatic Coordinate (GCC), as G/(R+G+B) from the Red, Green and Blue channels of the image (and BCC, RCC). 

See below for an example of the six images collected per ring, including four 'full view' images, and two 'zoomed' images (red boxes).

## View of the EucFACE from the security camera at Ring 6.

![](https://bitbucket.org/remkoduursma/facesecurecam_analysis/raw/master/docs/overviewring6.png)


## Instructions for use

To download and analyze the images, you need an API key for the HIEv, and [the HIEv R package](https://bitbucket.org/remkoduursma/HIEv). See there for instructions if you have not set this up before.

If the API key is set (or stored in the usual location), you should be able to do `source('run.R')`. This downloads images from the HIEv, unzips, analyzes, and uploads to the files :

- `FACE_P0037_RA_CANOPYGREENNESS-ZOOMED_OPEN_L2.dat`
- `FACE_P0037_RA_CANOPYGREENNESS-FULL_OPEN_L2.dat`

New data is appended to these files when they become available. 

## Contact 

Remko Duursma & Craig McNamara


## Online

See bitbucket.org/eucface for all scripts.
